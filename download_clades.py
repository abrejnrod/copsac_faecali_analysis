import pandas as pd
from ftplib import FTP
import os
ftp = FTP('ftp.ncbi.nlm.nih.gov')
ftp.login()
clades = pd.read_csv("clades.csv", sep = ";", skiprows = 3)

for gid in clades["Genome ID"]:
    t = gid.split("_")[1]
    t1 = t[0:3]
    t2 = t[3:6]
    t3 = t[6:9]
    giddir = "/genomes/all/GCA/%s/%s/%s/" %(t1, t2, t3)
    print(gid, t, t1, t2, t3, giddir)
    ex = 'wget -l 2 -R "*rna*, *cds*" -r -A genomic.fna.gz   ftp://ftp.ncbi.nlm.nih.gov' + giddir
    print(ex)
    os.system(ex)
